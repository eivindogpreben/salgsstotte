(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.webFontTxtInst = {}; 
var loadedTypekitCount = 0;
var loadedGoogleCount = 0;
var gFontsUpdateCacheList = [];
var tFontsUpdateCacheList = [];
lib.ssMetadata = [];



lib.updateListCache = function (cacheList) {		
	for(var i = 0; i < cacheList.length; i++) {		
		if(cacheList[i].cacheCanvas)		
			cacheList[i].updateCache();		
	}		
};		

lib.addElementsToCache = function (textInst, cacheList) {		
	var cur = textInst;		
	while(cur != null && cur != exportRoot) {		
		if(cacheList.indexOf(cur) != -1)		
			break;		
		cur = cur.parent;		
	}		
	if(cur != exportRoot) {		
		var cur2 = textInst;		
		var index = cacheList.indexOf(cur);		
		while(cur2 != null && cur2 != cur) {		
			cacheList.splice(index, 0, cur2);		
			cur2 = cur2.parent;		
			index++;		
		}		
	}		
	else {		
		cur = textInst;		
		while(cur != null && cur != exportRoot) {		
			cacheList.push(cur);		
			cur = cur.parent;		
		}		
	}		
};		

lib.gfontAvailable = function(family, totalGoogleCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], gFontsUpdateCacheList);		

	loadedGoogleCount++;		
	if(loadedGoogleCount == totalGoogleCount) {		
		lib.updateListCache(gFontsUpdateCacheList);		
	}		
};		

lib.tfontAvailable = function(family, totalTypekitCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], tFontsUpdateCacheList);		

	loadedTypekitCount++;		
	if(loadedTypekitCount == totalTypekitCount) {		
		lib.updateListCache(tFontsUpdateCacheList);		
	}		
};
// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(1,1,1).p("Egq9gfPMBV7AAAMAAAA+fMhV7AAAg");
	this.shape.setTransform(275,200);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#00CC33").s().p("Egq9AfQMAAAg+fMBV7AAAMAAAA+fg");
	this.shape_1.setTransform(275,200);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(-1,-1,552,402), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0066CC").s().p("AnAHBQi6i6AAkHQAAkGC6i6QC6i6EGAAQEHAAC6C6QC6C6AAEGQAAEHi6C6Qi6C6kHAAQkGAAi6i6g");
	this.shape.setTransform(63.5,63.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(0,0,127,127), null);


// stage content:
(lib.animationlib = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Symbol1();
	this.instance.parent = this;
	this.instance.setTransform(130.5,231.5,1,1,0,0,0,63.5,63.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({x:135.5,y:230.1},0).wait(1).to({x:140.4,y:228.7},0).wait(1).to({x:145.4,y:227.4},0).wait(1).to({x:150.4,y:226},0).wait(1).to({x:155.3,y:224.6},0).wait(1).to({x:160.3,y:223.2},0).wait(1).to({x:165.2,y:221.8},0).wait(1).to({x:170.2,y:220.5},0).wait(1).to({x:175.2,y:219.1},0).wait(1).to({x:180.1,y:217.7},0).wait(1).to({x:185.1,y:216.3},0).wait(1).to({x:190.1,y:214.9},0).wait(1).to({x:195,y:213.5},0).wait(1).to({x:200,y:212.2},0).wait(1).to({x:204.9,y:210.8},0).wait(1).to({x:209.9,y:209.4},0).wait(1).to({x:214.9,y:208},0).wait(1).to({x:219.8,y:206.6},0).wait(1).to({x:224.8,y:205.3},0).wait(1).to({x:229.8,y:203.9},0).wait(1).to({x:234.7,y:202.5},0).wait(1).to({x:239.7,y:201.1},0).wait(1).to({x:244.7,y:199.7},0).wait(1).to({x:249.6,y:198.4},0).wait(1).to({x:254.6,y:197},0).wait(1).to({x:259.6,y:195.6},0).wait(1).to({x:264.5,y:194.2},0).wait(1).to({x:269.5,y:192.8},0).wait(1).to({x:274.4,y:191.5},0).wait(1).to({x:279.4,y:190.1},0).wait(1).to({x:284.4,y:188.7},0).wait(1).to({x:289.3,y:187.3},0).wait(1).to({x:294.3,y:185.9},0).wait(1).to({x:299.3,y:184.5},0).wait(1).to({x:304.2,y:183.2},0).wait(1).to({x:309.2,y:181.8},0).wait(1).to({x:314.2,y:180.4},0).wait(1).to({x:319.1,y:179},0).wait(1).to({x:324.1,y:177.6},0).wait(1).to({x:329,y:176.3},0).wait(1).to({x:334,y:174.9},0).wait(1).to({x:339,y:173.5},0).wait(1).to({x:343.9,y:172.1},0).wait(1).to({x:348.9,y:170.7},0).wait(1).to({x:353.9,y:169.4},0).wait(1).to({x:358.8,y:168},0).wait(1).to({x:363.8,y:166.6},0).wait(1).to({x:368.7,y:165.2},0).wait(1).to({x:373.7,y:163.8},0).wait(1).to({x:378.7,y:162.5},0).wait(1).to({x:383.6,y:161.1},0).wait(1).to({x:388.6,y:159.7},0).wait(1).to({x:393.6,y:158.3},0).wait(1).to({x:398.5,y:156.9},0).wait(1).to({x:403.5,y:155.6},0).wait(1));

	// Layer 2
	this.instance_1 = new lib.Symbol2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(275.6,200.5,0.998,0.998,0,0,0,275.1,200);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(56));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(275.5,200.5,550,400);
// library properties:
lib.properties = {
	id: 'C642845BA1C46F47BF8CFFE7505D7F3E',
	width: 550,
	height: 400,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	webfonts: {},
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['C642845BA1C46F47BF8CFFE7505D7F3E'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;