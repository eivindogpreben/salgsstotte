(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.webFontTxtInst = {}; 
var loadedTypekitCount = 0;
var loadedGoogleCount = 0;
var gFontsUpdateCacheList = [];
var tFontsUpdateCacheList = [];
lib.ssMetadata = [];



lib.updateListCache = function (cacheList) {		
	for(var i = 0; i < cacheList.length; i++) {		
		if(cacheList[i].cacheCanvas)		
			cacheList[i].updateCache();		
	}		
};		

lib.addElementsToCache = function (textInst, cacheList) {		
	var cur = textInst;		
	while(cur != null && cur != exportRoot) {		
		if(cacheList.indexOf(cur) != -1)		
			break;		
		cur = cur.parent;		
	}		
	if(cur != exportRoot) {		
		var cur2 = textInst;		
		var index = cacheList.indexOf(cur);		
		while(cur2 != null && cur2 != cur) {		
			cacheList.splice(index, 0, cur2);		
			cur2 = cur2.parent;		
			index++;		
		}		
	}		
	else {		
		cur = textInst;		
		while(cur != null && cur != exportRoot) {		
			cacheList.push(cur);		
			cur = cur.parent;		
		}		
	}		
};		

lib.gfontAvailable = function(family, totalGoogleCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], gFontsUpdateCacheList);		

	loadedGoogleCount++;		
	if(loadedGoogleCount == totalGoogleCount) {		
		lib.updateListCache(gFontsUpdateCacheList);		
	}		
};		

lib.tfontAvailable = function(family, totalTypekitCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], tFontsUpdateCacheList);		

	loadedTypekitCount++;		
	if(loadedTypekitCount == totalTypekitCount) {		
		lib.updateListCache(tFontsUpdateCacheList);		
	}		
};
// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(1,1,1).p("Egq9gfPMBV7AAAMAAAA+fMhV7AAAg");
	this.shape.setTransform(275,200);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CCFF00").s().p("Egq9AfQMAAAg+fMBV7AAAMAAAA+fg");
	this.shape_1.setTransform(275,200);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(-1,-1,552,402), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0066CC").s().p("AnAHBQi6i6AAkHQAAkGC6i6QC6i6EGAAQEHAAC6C6QC6C6AAEGQAAEHi6C6Qi6C6kHAAQkGAAi6i6g");
	this.shape.setTransform(63.5,63.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(0,0,127,127), null);


// stage content:
(lib.animationlib = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Symbol1();
	this.instance.parent = this;
	this.instance.setTransform(106.8,189.4,0.818,0.818,0,0,0,63.5,63.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({x:110.8,y:188.3},0).wait(1).to({x:114.9,y:187.1},0).wait(1).to({x:118.9,y:186},0).wait(1).to({x:123,y:184.9},0).wait(1).to({x:127.1,y:183.7},0).wait(1).to({x:131.1,y:182.6},0).wait(1).to({x:135.2,y:181.5},0).wait(1).to({x:139.2,y:180.4},0).wait(1).to({x:143.3,y:179.2},0).wait(1).to({x:147.4,y:178.1},0).wait(1).to({x:151.4,y:177},0).wait(1).to({x:155.5,y:175.8},0).wait(1).to({x:159.5,y:174.7},0).wait(1).to({x:163.6,y:173.6},0).wait(1).to({x:167.7,y:172.5},0).wait(1).to({x:171.7,y:171.3},0).wait(1).to({x:175.8,y:170.2},0).wait(1).to({x:179.8,y:169.1},0).wait(1).to({x:183.9,y:167.9},0).wait(1).to({x:188,y:166.8},0).wait(1).to({x:192,y:165.7},0).wait(1).to({x:196.1,y:164.5},0).wait(1).to({x:200.1,y:163.4},0).wait(1).to({x:204.2,y:162.3},0).wait(1).to({x:208.3,y:161.2},0).wait(1).to({x:212.3,y:160},0).wait(1).to({x:216.4,y:158.9},0).wait(1).to({x:220.5,y:157.8},0).wait(1).to({x:224.5,y:156.6},0).wait(1).to({x:228.6,y:155.5},0).wait(1).to({x:232.6,y:154.4},0).wait(1).to({x:236.7,y:153.2},0).wait(1).to({x:240.8,y:152.1},0).wait(1).to({x:244.8,y:151},0).wait(1).to({x:248.9,y:149.8},0).wait(1).to({x:252.9,y:148.7},0).wait(1).to({x:257,y:147.6},0).wait(1).to({x:261.1,y:146.5},0).wait(1).to({x:265.1,y:145.3},0).wait(1).to({x:269.2,y:144.2},0).wait(1).to({x:273.2,y:143.1},0).wait(1).to({x:277.3,y:141.9},0).wait(1).to({x:281.4,y:140.8},0).wait(1).to({x:285.4,y:139.7},0).wait(1).to({x:289.5,y:138.5},0).wait(1).to({x:293.6,y:137.4},0).wait(1).to({x:297.6,y:136.3},0).wait(1).to({x:301.7,y:135.2},0).wait(1).to({x:305.7,y:134},0).wait(1).to({x:309.8,y:132.9},0).wait(1).to({x:313.9,y:131.8},0).wait(1).to({x:317.9,y:130.6},0).wait(1).to({x:322,y:129.5},0).wait(1).to({x:326,y:128.4},0).wait(1).to({x:330.1,y:127.2},0).wait(1));

	// Layer 2
	this.instance_1 = new lib.Symbol2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(225.4,164,0.817,0.816,0,0,0,275.1,200);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(56));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(225.4,164.4,450,327.3);
// library properties:
lib.properties = {
	id: 'D642845BA1C46F47BF8CFFE7505D7F3E',
	width: 450,
	height: 328,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	webfonts: {},
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['D642845BA1C46F47BF8CFFE7505D7F3E'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;