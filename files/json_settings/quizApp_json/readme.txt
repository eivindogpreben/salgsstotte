{
"settings": {
    "border_off": false,
    "navButtonText": "",
    "navButtonText_last": ""
},
"questions": [{
        "qType": "mc",
        "customHeading": "",
        "image": "img/bg-intro/bg-afr01.gif",
        "qText": "Hvilke ingredienser ville du ha brukt for å lage pannekaker?",
        "options": [
            {
                "option": "Egg",
                "correct": true,
                "feedback": "feedback for option 1"
            },
            {
                "option": "Melk",
                "correct": true,
                "feedback": "feedback for option 2"
            },
            {
                "option": "Potet",
                "correct": false,
                "feedback": "feedback for option 3"
            }
        ],
        "standard_feedback_on": true,
        "correct_feedback": "Min korrekte feedback.",
        "incorrect_feedback": "Min feile feedback.",
        "partlycorrect_feedback": "Min delvis feedback.",
        "common_feedback": "Felles feedback",
        "soundsetting": "off",
        "qButton_first": "Besvar",
        "qButton_second": "",
        "checkboxOverride": true,
        "twoCol": 50,
        "score": 1
        
    }
]
}

############################################
Settings
############################################

--------------------------------------------
border_off
--------------------------------------------
To turn off border and quiz icon
Optional
Default = true


--------------------------------------------
navButtonText/navButtonText_last
--------------------------------------------
To set custom text navigation button and the last navigation button

Optional


############################################
Individual questions
############################################



--------------------------------------------
qType:
--------------------------------------------
Type of question
Optional
Default = mc

mc, mc_open, og write
mc = multiple choice
mc_open = multiple choice, men har ikke noe rett eller galt svar
write = fylle inn tekst
textonly = hvis det bare er tekst som skal vises
fillin = hvis bruker skal skrive inn noe i et tekstfelt

--------------------------------------------
customHeading:
--------------------------------------------
Settings for custom heading
Optional
Default = "Oppgave"

"" = "Oppgave"
"off" = Turns it off

--------------------------------------------
image:
--------------------------------------------
Path to a image (from root of project)

Optional

--------------------------------------------
qText:
--------------------------------------------
The question

--------------------------------------------
options:
--------------------------------------------
Options in multiple choice. 

If multiple options is true = checkboxes
If only one option is true = radiobuttons
"feedback" makes it possble to set feedback on the alternatives selected. Is optional.

--------------------------------------------
standard_feedback_on:
--------------------------------------------
Gives a standard feedback if true. 
Turns off standard feedback if false.

Optional
Default = true

--------------------------------------------
correct_feedback/incorrect_feedback/partlycorrect_feedback:
--------------------------------------------
Feedback for correct answer, incorrect answer and partly correct answer

If partlycorrect_feedback is not set or sett to "off", the application writes the incorrect_feedback

Optional

--------------------------------------------
common_feedback:
--------------------------------------------
Feedback common for both correct and incorrect answer

Optional

--------------------------------------------
soundsetting:
--------------------------------------------

"soundsetting": "off",

Optional
Default = true
off  // turns sound off
partlytowrong // sets partly correct answers to wrong sound
on or any value turns sound on

May also be set in settings


--------------------------------------------
qButton_first/qButton_second
--------------------------------------------
To set custom text on the buttons that is not navigation button. Typical "sjekk svar" og "prøv igjen"

Optional

--------------------------------------------
checkboxOverride
--------------------------------------------
If only one option is correct radiobuttons is used. This option uses checkboxes instead

Optional


--------------------------------------------
twoCol
--------------------------------------------
Enable twoCol question, a number that indicates the width in percentage of left column, right column is caclulated. If its not a number it is turned off.

Default = off

Optional

--------------------------------------------
score
--------------------------------------------
Sets the score of the question. The number is stored if the question is right. If its not a number it is turned off.
The score may be retrieved from strings with @total and @totalscore

Default = off
Optional

