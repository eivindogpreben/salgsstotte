////// Use this file to configure the project
// Write here what version of ng1-lib you are using:
// ng-lib version 2.0.4

//// Set the name of project folder
var nameOfProjectFolder = "default";

//// Set up quizApp
var standalonemode = false;
var quizAppFolderPath= './ng1-lib/angularjs/quizApp/';
var jsonfiles = './../../../json_settings/quizApp_json/';


//// Set up regnestykkeApp
var regnestykkeAppFolderPath= './ng1-lib/angularjs/regnestykkeApp/';
var jsonfiles_regnestykke = './../../../json_settings/regnestykkeApp_json/';

//// Set up videomenu
var videomenuFolderPath= './ng1-lib/angularjs/videomenu/';
var jsonfiles_videomenu = './../../../json_settings/videomenu_json/';


/*---------------------------------------*/
/*  On document ready
/*  $(document).ready(); 
/*---------------------------------------*/

 $(function() {

    //// Make navbar dropdown menu

    MyProject.navbar_dropdown_item($('.code_to_menu'), 'index.html', '','<i class="fa fa-arrow-left"></i> Gå til startsiden');
    MyProject.navbar_dropdown_item($('.code_to_menu'), 'elements.html', '' ,'Elements');
    MyProject.navbar_dropdown_item($('.code_to_menu'), 'intro_video.html', '' ,'Intro: type video');
    MyProject.navbar_dropdown_item($('.code_to_menu'), 'intro_type2.html', '' ,'Intro: type 2');
    MyProject.navbar_dropdown_item($('.code_to_menu'), 'info.html', '' ,'Project info');

});

/*---------------------------------------*/
/* On window load
/* When everything is loaded
/*---------------------------------------*/
$(window).load(function(){

    // init animations
     initAdobeAnimate("#animation1","canvas","animation_container","C642845BA1C46F47BF8CFFE7505D7F3E","dom_overlay_container, 550, 400");
     initAdobeAnimate("#animation2","canvas2","animation_container2","D642845BA1C46F47BF8CFFE7505D7F3E","dom_overlay_container2, 450, 328");

});


// demonstrate how to use handlebars

var postTemplate = hb['test'];
console.log(postTemplate({
  firstname: 'Eivind', 
  lastname: 'Venas'
}));

console.log(hb['test']({
  firstname: 'Preben', 
  lastname: 'Soot'
}));


