module.exports = function(grunt) {
  // Do grunt-related things in here
  // Project configuration.


  var globalConfig = {
		files: 'files',
    files_to_prod: ['**/*'],
    prod: 'prod',
    resources: 'resources',
    statics: 'statics',
    ng1_lib_short: 'ng1-lib',	
    ng1_lib: 'statics/ng1-lib'
  };

  ////////////////////////////////////
  // All configuration goes here
  ////////////////////////////////////

  grunt.initConfig({

    globalConfig: globalConfig,

    less: {

      customstyles:{
        options: {
          sourceMap: true,
          outputSourceFiles: true,
          sourceMapURL: 'projectstyles.css.map',
          sourceMapFilename: '<%= globalConfig.prod %>/projectstyles.css.map'
        },
        files: {
          '<%= globalConfig.prod %>/projectstyles.css': '<%= globalConfig.resources %>/projectstyles.less'
        }
      }
    },


    // concats scripts in resources
    concat: {
			dist: {
				src: [
					'<%= globalConfig.resources %>/project_config.js',
					'<%= globalConfig.resources %>/adhocjs.js',
					
				],
				dest: '<%= globalConfig.prod %>/scripts.js',
			}
		},

    // copies only what is changed
		sync: {
			files_folder: {
				expand: true,
				cwd: '<%= globalConfig.files  %>/',
				src: '<%= globalConfig.files_to_prod %>',
				dest: '<%= globalConfig.prod %>/'
			},
      statics_folder: {
				expand: true,
				cwd: '<%= globalConfig.ng1_lib  %>/',
				src: '**/*',
				dest: '<%= globalConfig.prod %>/<%= globalConfig.ng1_lib_short %>'
			}
		},

    // watches files
    // watches ng1_lib less so its possible to work on lib and see changes directly in 01-standard-project
    esteWatch: {
    	options: {
				dirs: ['<%= globalConfig.files %>/**/', '<%= globalConfig.ng1_lib %>/less/**/', '<%= globalConfig.ng1_lib %>/angularjs/quizApp/less/**/', '<%= globalConfig.resources %>/**/', '<%= globalConfig.ng1_lib %>/**/' ],
        livereload: {
          enabled:false
        }
			},

			"*": function(filepath){
				return ['less', 'concat', 'sync'];
			}
    },
    
   
		

    copy: {
      statics:{
        files:[
          {
            cwd: '<%= globalConfig.statics %>/',
            expand: true,
            src: ['**'],
            dest: '<%= globalConfig.prod %>/'
          }
        ]
      },
      files:{
        files:[
          {
            cwd:'<%= globalConfig.files %>/',
            expand: true,
            src: ['**'],
            dest: '<%= globalConfig.prod %>/'
          }
        ]
      }
    }		
  });
	
  ////////////////////////////////////////////////////////////////////////
  // Where we tell Grunt we plan to use this plug-in.
  ////////////////////////////////////////////////////////////////////////


  grunt.loadTasks('node_modules/grunt-este-watch/tasks');
  grunt.loadTasks('node_modules/grunt-contrib-less/tasks');
  grunt.loadTasks('node_modules/grunt-contrib-copy/tasks');
  grunt.loadTasks('node_modules/grunt-sync/tasks');
  grunt.loadTasks('node_modules/grunt-contrib-concat/tasks');
  
  /* If this projectfolder is used standalone:
  
      1. rename "package(if_standalone).json" --> "package.json"
      2. npm install
      3. remove "../" from the five grunt.loadTasks(), like this: grunt.loadTasks('node_modules/grunt-sync/tasks'):  
      4. run grunt to compile less, and move files to prod
  */

  /////////////////////////////////////////////////////////////////////////////
  // Where we tell Grunt what to do when we type "grunt" into the terminal.
  /////////////////////////////////////////////////////////////////////////////
  
  //setup prod-folder - sets it up with basic files
  //run this before you start default grunt
  grunt.registerTask('prod', ['less', 'concat','copy']);

  //watch and move things to prod
  grunt.registerTask('default', ['less',  'concat', 'copy', 'esteWatch']);


};