mainApp.controller('videomenuController', ['$scope', '$http', '$location', '$log', '$animate', '$sce', function($scope, $http, $location, $log, $animate, $sce) {
    
   
    $log.info("---------videomeny-------------");
    $scope.infotext = "";


    // HENT JSON

 
    $http.get(videomenuFolderPath + jsonfiles_videomenu + $scope.data + ".json")
        .success(function(result) {

            $scope.settings = result.settings;
            $scope.menuitems = result.menuitems;

            $scope.init();

        })
        .error(function(data, status) {
            console.log(data);
        });

  
   $scope.trustSrc = function(src) {
    return $sce.trustAsResourceUrl(src);
  }
  
   
    // INITIALIZES A QUESTION
    $scope.init = function() {
       $scope.videosrc = $scope.menuitems[0].link;
    };


    $scope.onInputChange = function(theIndex) {
        $scope.videosrc = $scope.menuitems[theIndex].link;
    }

    $scope.onMouseOver = function(theIndex){
        $scope.infotext = $scope.menuitems[theIndex].infotext;
    }

     $scope.onMouseleave = function(theIndex){
        $scope.infotext="";
    }


     

       

}]);






mainApp.directive("videomenu", function() {
    
    
    return {
        templateUrl: videomenuFolderPath + 'templates/videoMenuTemplate.html',
        replace: true,
        scope: {
            data: '='
        },
        controller: 'videomenuController'

    };
});
