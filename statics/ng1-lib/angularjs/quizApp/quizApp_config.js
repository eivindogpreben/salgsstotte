var standalonemode = true;
var quizAppFolderPath= './';
var jsonfiles = 'standalone_files_custom/json/';


//var quizAppFolderPath= 'angularjs/quizApp/';

// mainApp must be a part of the HTML-file
if(standalonemode){
    var mainApp=angular.module('mainApp', ['ngSanitize', 'ngAnimate']);
}

/* HOW TO USE 

- Install packages.json
- Adjust settings in this file
- Adjust standalone_files_custom/customstyles.less
- grunt

*/ 