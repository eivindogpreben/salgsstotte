module.exports = function(grunt) {
  // Do grunt-related things in here
  // Project configuration.


	var globalConfig = {
    src: 'less',
    dest: 'css'
  };

  ////////////////////////////////////
  // All configuration goes here
  ////////////////////////////////////

  grunt.initConfig({

		globalConfig: globalConfig,

    // May use this to reuse properties. Like: pkg.name
    pkg: grunt.file.readJSON('package.json'),


    concat: {
      scripts_static: {
			  src: [
			  	// node_modules
			    'node_modules/jquery/dist/jquery.min.js',
			    'node_modules/bootstrap/dist/js/bootstrap.min.js',
			    'node_modules/angular/angular.min.js',
			    'node_modules/angular-route/angular-route.min.js',
					'node_modules/angular-sanitize/angular-sanitize.min.js',
					'node_modules/angular-animate/angular-animate.min.js'
			    ],
			    dest: 'standalone_files/scripts_static.js'
			},
			css_static: {
			  src: [
			    'node_modules/font-awesome/css/font-awesome.min.css',
			    'node_modules/bootstrap/dist/css/bootstrap.min.css'
			    ],
			    dest: 'standalone_files/css/styles_static.css'
			}
		},

     copy: {
      
			fromModules:{
			  files:[
			    {
			      cwd:'node_modules/font-awesome/fonts/',
			      expand: true,
			      src: ['**'],
			      dest: 'standalone_files/fonts/'
			    }
			  ]
			}
     },

    // compile less stylesheets to css -----------------------------------------

    less: {

      angularjs_quizapp:{
        options: {
          sourceMap: true,
          outputSourceFiles: true,
        },
        expand: true,
        cwd:"<%= globalConfig.src  %>/",
        src: "*.less",
        dest: "<%= globalConfig.dest  %>/",
        ext: ".css"
      },
      customstyles:{
        options: {
          sourceMap: true,
          outputSourceFiles: true,
          sourceMapURL: 'customstyles.css.map',
          sourceMapFilename: 'standalone_files_custom/customstyles.css.map'
        },
        files: {
          'standalone_files_custom/customstyles.css': 'standalone_files_custom/customstyles.less'
        }
      }
		},

		 watch: {
    	all: {
    		// if any file changes in the directory "<%= globalConfig.src  %>/"
      	files: ['<%= globalConfig.src  %>/**/*', 'standalone_files_custom/**/*'],
      	tasks: ['less']
    	}
		 }
		
  });
	
  ////////////////////////////////////////////////////////////////////////
  // Where we tell Grunt we plan to use this plug-in.
  ////////////////////////////////////////////////////////////////////////

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-copy');
  

  //////////////////////////////////////////////////////////////////////////////
  // Where we tell Grunt what to do when we type "grunt" into the terminal.
  //////////////////////////////////////////////////////////////////////////////
  
  //watch everything and compiles less and scripts
  grunt.registerTask('default', ['concat', 'less', 'copy', 'watch']);


};