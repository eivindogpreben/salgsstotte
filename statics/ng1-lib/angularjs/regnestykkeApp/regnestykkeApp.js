mainApp.controller('regnestykkeController', ['$scope', '$http', '$location', '$log', '$animate', function ($scope, $http, $location, $log, $animate) {

        $scope.myVar = "myVar2";

        $http.get(regnestykkeAppFolderPath + jsonfiles_regnestykke + $scope.data + ".json")
                .success(function (result) {
                    $scope.regnestykke = result;
                    console.log($scope.regnestykke);
                    $scope.init();

                })
                .error(function (data, status) {
                    console.log(data);
                });


        $scope.init = function () {
            $scope.tittel = $scope.regnestykke[0].tittel;
            $scope.linjer = $scope.regnestykke[0].linjer;
        }


    }]);



mainApp.directive("regnestykke", function () {
    return{
        templateUrl: regnestykkeAppFolderPath + 'templates/regnestykke.html',
        replace: true,
        scope: {
            data: '='
        },
        controller: 'regnestykkeController'

    }
});


