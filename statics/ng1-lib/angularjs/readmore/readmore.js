mainApp.controller('readmore_controller', ['$scope', '$http', '$location', '$log', '$animate', function ($scope, $http, $location, $log, $animate) {

    var defaultButtonText="Les mer";
    var theButtonText=defaultButtonText;
        
    if($scope.buttonText===undefined){
        theButtonText = defaultButtonText;
    }else{
        theButtonText = $scope.buttonText;
    }
    
    $scope.printedButtonText = theButtonText;
    
    
    $scope.showText = false;
    
    $scope.toggle = function (){
        if($scope.showText){
            $scope.showText = false;
            $scope.printedButtonText = theButtonText;
        }else{
            $scope.showText = true;
            $scope.printedButtonText = "";
        }
    }
   
    }]);



mainApp.directive("readmore", function () {
    return{
        templateUrl: './ng1-lib/angularjs/readmore/templates/readmore.html',
        replace: true,
        transclude: true,
        scope: {
            buttonText: '='
        },
        controller: 'readmore_controller'

    };
});




