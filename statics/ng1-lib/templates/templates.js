this["hb"] = this["hb"] || {};

this["hb"]["adobeAnimate"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div id=\""
    + alias4(((helper = (helper = helpers.hbAnimation_container || (depth0 != null ? depth0.hbAnimation_container : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"hbAnimation_container","hash":{},"data":data}) : helper)))
    + "\" style=\"background-color:rgba(255, 255, 255, 1.00); width:"
    + alias4(((helper = (helper = helpers.hbWidth || (depth0 != null ? depth0.hbWidth : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"hbWidth","hash":{},"data":data}) : helper)))
    + "px; height:"
    + alias4(((helper = (helper = helpers.hbHeight || (depth0 != null ? depth0.hbHeight : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"hbHeight","hash":{},"data":data}) : helper)))
    + "px; position:relative; margin:auto;\">\r\n\r\n  <canvas id=\""
    + alias4(((helper = (helper = helpers.hbCanvas || (depth0 != null ? depth0.hbCanvas : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"hbCanvas","hash":{},"data":data}) : helper)))
    + "\" width=\""
    + alias4(((helper = (helper = helpers.hbWidth || (depth0 != null ? depth0.hbWidth : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"hbWidth","hash":{},"data":data}) : helper)))
    + "\" height=\""
    + alias4(((helper = (helper = helpers.hbHeight || (depth0 != null ? depth0.hbHeight : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"hbHeight","hash":{},"data":data}) : helper)))
    + "\" style=\"position: absolute; display: block; background-color:rgba(255, 255, 255, 1.00);\"></canvas>\r\n\r\n  <div id=\""
    + alias4(((helper = (helper = helpers.hbDom_overlay_container || (depth0 != null ? depth0.hbDom_overlay_container : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"hbDom_overlay_container","hash":{},"data":data}) : helper)))
    + "\" style=\"pointer-events:none; overflow:hidden; width:"
    + alias4(((helper = (helper = helpers.hbWidth || (depth0 != null ? depth0.hbWidth : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"hbWidth","hash":{},"data":data}) : helper)))
    + "px; height:"
    + alias4(((helper = (helper = helpers.hbHeight || (depth0 != null ? depth0.hbHeight : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"hbHeight","hash":{},"data":data}) : helper)))
    + "px; position: absolute; left: 0px; top: 0px; display: block;\"></div>\r\n  \r\n</div>";
},"useData":true});

this["hb"]["test"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<h1>Handlebars working: "
    + alias4(((helper = (helper = helpers.firstname || (depth0 != null ? depth0.firstname : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"firstname","hash":{},"data":data}) : helper)))
    + " "
    + alias4(((helper = (helper = helpers.lastname || (depth0 != null ? depth0.lastname : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"lastname","hash":{},"data":data}) : helper)))
    + "</h1>";
},"useData":true});