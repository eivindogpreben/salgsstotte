# Version of ng1-lib 
1.9.2


# A new release should be found the git logg. Use this command: (ctrl+alt+shift+<)
git add .
git commit -m "ng1-lib update: version 1.9.2"
git tag -a v1.9.2 -m "Version 1.9.2"

# Make a lib-release accessible online by copying it (prod version) to ../versions

# When quizApp is updated, also update the quizApp-versjonumber here: angularjs/quizApp/package.json




##########################################
History of major/minor changes (ng1-lib)
##########################################

# 1.9.2
- added bootstrap-theme
- changes to button less

# 1.9.1
- AOS anim on scroll
- removed a unused components and plugins or moved them to node_modules

# 1.9.0
- Major changes to section and intro-styles, making it more flexible
- Changes to bgpatterns and bgimage, making them more flexible

# 1.8.5
- Handlebars and Adobe Animate with handlebars

# 1.8.4
- Implemented Adobe Animate

# 1.8.0
- moved colors out of main, several less changes may affect earlier versions

# 1.7.6
- moved web.config

# 1.7.5
- more flexible introtitle

# 1.7.2
- removed automatic quiz anims, will affect earlier versions. 
- add dialog testimonials

# 1.6.1
- calculate headings + equal content and section ingress

# 1.6.0
- large reorganization of titles and snippets

# 1.5.0
- reorganized javascript files
- URL parameter fucntions
- cleaned navbar and intro less

# 1.4.2
- content-box with thumbnail
- removeIndent on timeline

# 1.4.0
- structural changes to less and elements
- may affect earlier versions

# 1.3.6
- small patches from 1.3.2

# 1.3.2
- more structural changes
- solved problems with package.json


# 1.3.0
- structural changes on margins and headings
- removed angular2 totally

# 1.2.2
- do not cache json quiz

# 1.2.1
- quizApp bugfix, feedback_option

# 1.2.0
- added intro type 2
- cleaned intro styles

# 1.1.6
- quizApp: center image
- comment out ng2 in gruntfile

# 1.1.5
- quizApp possible to have feedback on each option

# 1.1.5
- quizApp possible to have feedback on each option

# 1.1.4
- Added videobg

# 1.1.3
- Added section-bgimage-yes & section-bgpattern-yes

# 1.1.2
- Contact details in footer

# 1.1.0
- Lagt inn enkel tincan
- Deaktivert ng2

# 1.1.0
- Cleaned up less variables

# 1.0.3
- Uglify scripts in prod

# 1.0.1
- All elements except title needs topmargin-standard

# 1.0.0
- Changed name: spa-learning-ng1 --> spa-learning
- Started versioning from scratch 6.0.0 --> 1.0.0


##########################################
History of major/minor changes (ng2-lib)
##########################################

# 0.0.0


##########################################
About versioning in this project
##########################################

x.x.x = major.minor.patch

Major version 
Minor version 
Patch 

Major: "when you make incompatible changes"
Spa-learning-definition:
- Inputs like json-files may not work
- Highly likely that functionality has changed and bugs appears when updating
- Highly likely that layout has changed

Minor: "when you add functionality in a backwards-compatible manner"
Spa-learning-definition:
- Inputs, like json-files, is working, but may look and behave different than previous versions
- Check layout and functionality

Patch: "when you make backwards-compatible bug fixes"
Spa-learning-definition:


